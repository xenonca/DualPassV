local ms = minetest.get_mod_storage()

--- FORMSPECS ---
local function entry_fs(name, entrytype)
	local img = "dualpassv_dpv_banner.png"
	local btn = "submit;SUBMIT"
	if entrytype == "newpw" then
		img = "dualpassv_pw_q_banner.png"
		btn = "set;SET"
	end

	local formspec = {
        "formspec_version[4]",
        "size[12,6]",
        "image[2.602,1;6.796,0.876;", img, "]",
        "pwdfield[3,3;6,0.8;pwd;Password]",
        "button[4.5,4.4;3,0.8;", btn, "]",
		"field_close_on_enter[pwd;false]"
    }
    return table.concat(formspec, "")
end

local function show_fs(name, entrytype)
	local text = "label[5.4,0.7;Test Page]"
	local img = "dualpassv_pw_invchars.png"
	local btn = "button_exit[4.5,4.4;3,0.8;exit;EXIT]"
	local app = ""

	if entrytype == "verified" then
		text = "label[5.4,0.7;Verified!]"
		img = "dualpassv_unlocked_v.png"
		btn = "button_exit[4.5,4.4;3,0.8;done;Done!]"

	elseif entrytype == "invchars" then
		text = "label[3.8,0.7;Your password cannot be empty!]"
		img = "dualpassv_pw_invchars.png"
		btn = "button_exit[4.5,4.4;3,0.8;ok;Ok!]"

	elseif entrytype == "nopw" then
		text = "label[4,0.7;No verification password set!]"
		img = "dualpassv_unlocked_x.png"
		btn = "button[3,4.4;3,0.8;set;SET]"
		app = "button_exit[6.25,4.4;3,0.8;dismiss;Dismiss]"

	elseif entrytype == "notverified" then
		text = "label[3.2,0.7;Password change denied! Verify first!]"
		img = "dualpassv_locked_x.png"
		btn = "button_exit[4.5,4.4;3,0.8;ok;Ok!]"

	elseif entrytype == "samepw" then
		text = "label[3.2,0.7;New password cannot be the old one!]"
		img = "dualpassv_pw_x.png"
		btn = "button_exit[4.5,4.4;3,0.8;ok;Ok!]"

	elseif entrytype == "pwset" then
		text = "label[4.1,0.7;Verification password set!]"
		img = "dualpassv_pw_v.png"
		btn = "button_exit[4.5,4.4;3,0.8;ok;Ok!]"

	elseif entrytype == "pause" then
		text = "label[4.6,0.7;Verification paused!]"
		img = "dualpassv_locked_v.png"
		btn = "button[3,4.4;3,0.8;verify;Verify]"
		app = "button_exit[6.25,4.4;3,0.8;ok;Ok!]"
	end

	local formspec = {
        "formspec_version[4]",
        "size[12,6]",
		text,
        "image[5.1075,1.3;1.785,2.481;", img, "]",
    	btn,
		app
    }
    return table.concat(formspec, "")
end
--- ONJOIN ---
minetest.register_on_joinplayer(function(player, last_login)
	local name = player:get_player_name()
	local ver = ms:get_int(name)
	if ver == 1 or ver == 2 then
		if ver == 1 then
			ms:set_int(name, 2)
		end
		player:set_physics_override({
			speed = 0,
			jump = 0,
			gravity = 0
		})
		local oprivs = minetest.privs_to_string(minetest.get_player_privs(name))
		ms:set_string(name .. "_oprivs", oprivs)
		minetest.set_player_privs(name, {})
		minetest.chat_send_player(name, "Verification...")
		minetest.show_formspec(name, "dualpassv:pw", entry_fs(name, "pw"))
		minetest.after(15, function()
			if ms:get_int(name) == 1 then
				minetest.log("info", "[DualPassV]: " .. name .. " joined and verified")
			elseif ms:get_int(name) == 2 then
				local oprivs = minetest.string_to_privs(ms:get_string(name .. "_oprivs"))	
				if oprivs ~= nil then
					minetest.set_player_privs(name, oprivs)
					ms:set_string(name .. "oprivs", "")
				end
				if player:is_player() then
					minetest.kick_player(name, "Verification failed!")
				end
				minetest.log("info", "[DualPassV]: " .. name .. " joined but could not verify, kicked")
			else
				minetest.log("info", "[DualPassV]: " .. name .. "joined and verified but cleared their pw")
			end
		end)
	else -- ver = 0 or nil
		minetest.show_formspec(name, "dualpassv:nopw", show_fs(name, "nopw"))
	end
end)

minetest.register_on_leaveplayer(function(player, timed_out)
	local name = player:get_player_name()	
	local oprivs = minetest.string_to_privs(ms:get_string(name .. "_oprivs"))	
	if oprivs ~= nil then
		minetest.set_player_privs(name, oprivs)
		ms:set_string(name .. "oprivs", "")
	end
end)

--- COMMANDS ---
minetest.register_chatcommand("v", {
	description = "Verify your verification password",
	func = function(name)
		local ver = ms:get_int(name)
		if ver ~= 2 and ver ~= 1 then
			minetest.show_formspec(name, "dualpassv:newpw", entry_fs(name, "newpw"))
		else
			minetest.show_formspec(name, "dualpassv:pw", entry_fs(name, "pw"))
		end
	end
})

minetest.register_chatcommand("vset", {
	description = "Set your verification password",
	func = function(name)
	minetest.show_formspec(name, "dualpassv:newpw", entry_fs(name, "newpw"))
	end
})

minetest.register_chatcommand("vclear", {
	description = "Clear your own or a player's verification password",
	params = "[<player>]",
	func = function(name, player)
		local list = minetest.deserialize(ms:get("dpv")) or {}
		if player:trim() == "" or player == name then
			list[name] = nil
			ms:set_string("dpv", minetest.serialize(list))
			ms:set_int(name, 0)
			minetest.log("info", "[DualPassV]: " .. name .. " cleared " .. player .. "'s pw")
			return true, "Verification password cleared (discouraged)!"
		elseif player ~= name then
			if minetest.check_player_privs(name, "server") then
				if not minetest.player_exists(player) then
					return false, "No such player!"
				end
				list[player] = nil
				ms:set_string("dpv", minetest.serialize(list))
				ms:set_int(player, 0)
				minetest.log("info", "[DualPassV]: " .. name .. " cleared " .. player .. "'s pw")
				return true, "Verification password of " .. player .. " cleared!"
			else
				minetest.log("info", "[DualPassV]: " .. name .. " tried to clear " .. player .. "'s pw, denied")
				return false, "Insufficient privileges!"
			end
		else
			minetest.log("info", "[DualPassV]: " .. name .. " tried to clear " .. player .. "'s pw, error")
			return false, "Something went wrong..."
		end
	end
})

minetest.register_chatcommand("vpause", {
	description = "Pause your own or a player's verification (they/you need to verify again)",
	params = "[<player>]",
	func = function(name, player)
		if player:trim() == "" or player == name then
			local info = minetest.get_player_information(name)
			if info.connection_uptime < 20 then
				return false, "Cannot pause verification yet!\nPlease try again in 20s"
			end
			local nplayer = minetest.get_player_by_name(name)
			nplayer:set_physics_override({
				speed = 0,
				jump = 0,
				gravity = 0
			})
			local oprivs = minetest.privs_to_string(minetest.get_player_privs(name))
			ms:set_string(name .. "_oprivs", oprivs)
			minetest.set_player_privs(name, {})
			ms:set_int(name, 2)
			minetest.log("info", "[DualPassV]: " .. name .. " paused " .. player .. "'s verification")
			minetest.show_formspec(name, "dualpassv:pause", show_fs(name, "pause"))
		elseif player ~= name then
			if minetest.check_player_privs(name, "server") then
				if not minetest.get_player_by_name(player) then
					return false, "No such player online!"
				end
				local info = minetest.get_player_information(player)
				if info.connection_uptime < 20 then
					return false, "Cannot pause verification yet!\nPlease try again in 20s"
				end
				local nplayer = minetest.get_player_by_name(player)
				nplayer:set_physics_override({
					speed = 0,
					jump = 0,
					gravity = 0
				})
				local oprivs = minetest.privs_to_string(minetest.get_player_privs(player))
				ms:set_string(player .. "_oprivs", oprivs)
				minetest.set_player_privs(player, {})
				ms:set_int(player, 2)
				minetest.log("info", "[DualPassV]: " .. name .. " paused " .. player .. "'s verification")
				minetest.show_formspec(player, "dualpassv:pause", show_fs(player, "pause"))
			else
				minetest.log("info", "[DualPassV]: " .. name .. " tried to pause " .. player .. "'s verification, denied")
				return false, "Insufficient privileges!"
			end
		else
			minetest.log("info", "[DualPassV]: " .. name .. " tried to pause " .. player .. "'s verification, error")
			return false, "Something went wrong..."
		end
	end
})

--- FS FIELDS ---
minetest.register_on_player_receive_fields(function(player, formname, fields)
	local name = player:get_player_name()

    if formname == "dualpassv:pw" then
		if fields.submit then
			local list = minetest.deserialize(ms:get("dpv")) or {}
			local ver = ms:get_int(name)
			local pw = minetest.sha1(fields.pwd)

			if ver == 2 or ver == 1 then
				local ppw = list[name]
				if pw == ppw then
					if ver == 2 then
						local oprivs = minetest.string_to_privs(ms:get_string(name .. "_oprivs"))
						if oprivs ~= nil then
							minetest.set_player_privs(name, oprivs)
							ms:set_string(name .. "oprivs", "")
						end
						player:set_physics_override({
							speed = 1,
							jump = 1,
							gravity = 1
						})
						ms:set_int(name, 1)
						minetest.log("info", "[DualPassV]: " .. name .. " verified")
						minetest.show_formspec(name, "dualpassv:verified", show_fs(name, "verified"))
						minetest.chat_send_player(name, minetest.colorize("#00FF00", "Verified!"))
					elseif ver == 1 then
						minetest.log("info", "[DualPassV]: " .. name .. " verified again")
						minetest.show_formspec(name, "dualpassv:verified", show_fs(name, "verified"))
						minetest.chat_send_player(name, "Already verified!")
					end
				else
					local oprivs = minetest.string_to_privs(ms:get_string(name .. "_oprivs"))	
					if oprivs ~= nil then
						minetest.set_player_privs(name, oprivs)
						ms:set_string(name .. "oprivs", "")
					end
					ms:set_int(name, 2)
					minetest.log("info", "[DualPassV]: " .. name .. " could not verify, kicked")
					minetest.kick_player(name, "Verification failed!")
				end
			else -- ver = 0 or nil
				minetest.show_formspec(name, "dualpassv:nopw", show_fs(name, "nopw"))
			end
		end

	elseif formname == "dualpassv:newpw" then
		if fields.set then
			if fields.pwd == "" then
				minetest.show_formspec(name, "dualpassv:invchars", show_fs(name, "invchars"))
				return false
			end
	
			local list = minetest.deserialize(ms:get("dpv")) or {}
			local ver = ms:get_int(name)
			local pw = minetest.sha1(fields.pwd)
			local tempv = " changed their pw"
			
			if ver == 2 then
				minetest.log("info", "[DualPassV]: " .. name .. " pw change denied, not verified")
				minetest.show_formspec(name, "dualpassv:notverified", show_fs(name, "notverified"))
			else
				if pw == list[name] then
					minetest.show_formspec(name, "dualpassv:samepw", show_fs(name, "samepw"))
					return
				end
				list[name] = pw
				ms:set_string("dpv", minetest.serialize(list))
				if ver ~= 1 then
					tempv = " set up a pw"
					ms:set_int(name, 1)
				end
				minetest.log("info", "[DualPassV]: " .. name .. tempv)
				minetest.show_formspec(name, "dualpassv:pwset", show_fs(name, "pwset"))
			end
		end

	elseif formname == "dualpassv:nopw" then
		if fields.set then
			minetest.show_formspec(name, "dualpassv:newpw", entry_fs(name, "newpw"))
		end
	elseif formname == "dualpassv:pause" then
		if fields.verify then
			minetest.show_formspec(name, "dualpassv:pw", entry_fs(name, "pw"))
		end
	end
end)