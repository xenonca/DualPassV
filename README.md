# - DualPassV -
This mod aims to offer players a little more security over their account by requiring a second, so called, verification password in order to play.

## Description
DualPassV allows players to set up a verification password which needs to be entered before being able to play.  
`/v`: Verify your verification password  
`/vset`: Set your verification password  
`/vclear [<player>]`: Clear your verification password (or the one from another player if you have the server priv)  
`/vpause [<player>]`: Pause your verification status (or the one from another player if you have the server priv) You will have to verify again if you want to continue playing  

## License
Code: LGPL-2.1-only by xenonca  
Media: CC-BY-SA 4.0 by xenonca  

## Version
1.0